#!/usr/bin/env bash

apt-get update
apt-get install -y build-essential
apt-get install -y autoconf
apt-get install -y automake
apt-get install -y autopoint
apt-get install -y intltool
apt-get install -y libtool
apt-get install -y libglib2.0-dev
apt-get install -y libpng12-dev
apt-get install -y libgc-dev
apt-get install -y libfreetype6-dev
apt-get install -y liblcms2-dev
apt-get install -y libgtkmm-2.4-dev
apt-get install -y libxslt1-dev
apt-get install -y libboost-dev
apt-get install -y libpopt-dev
apt-get install -y libgsl0-dev
apt-get install -y libaspell-dev

apt-get install -y cmake
apt-get install -y libpango1.0-dev

apt-get install -y libpoppler-dev
apt-get install -y libpoppler-glib-dev
apt-get install -y graphicsmagick-libmagick-dev-compat
apt-get install -y libgnome-vfsmm-2.6-dev
apt-get install -y libssl-dev
apt-get install -y libwpg-dev
apt-get install -y libcdr-dev
apt-get install -y libvisio-dev

apt-get install -y libsoup2.4
apt-get install -y libgtkmm-3.0-dev
apt-get install -y libgstreamermm-1.0-dev

add-apt-repository ppa:cairo-dock-team/ppa
apt-get update
apt-get install -y cairo-dock
apt-get install -y cairo-dock-plug-ins

echo 'alias sourceprof="source ~/.bash_profile"' >/home/vagrant/.bash_profile
echo 'alias prof="sudo emacs ~/.bash_profile"' >>/home/vagrant/.bash_profile
echo 'alias cloneInkscape="sudo git clone --recursive https://gitlab.com/inkscape/inkscape"' >>/home/vagrant/.bash_profile
