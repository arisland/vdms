#!/usr/bin/env bash

apt-get install -y packaging-dev
apt-get install -y pkg-config
apt-get install -y python-dev
apt-get install -y libpango1.0-dev
apt-get install -y libglib2.0-dev
apt-get install -y libxml2-dev
apt-get install -y giflib-dbg
apt-get install -y libjpeg-dev
apt-get install -y libtiff-dev
apt-get install -y uthash-dev
apt-get install -y libspiro-dev
apt-get install -y build-essential
apt-get install -y automake
apt-get install -y flex
apt-get install -y bison

apt-get install -y unifont;

mkdir -p /home/vagrant/git
cd /home/vagrant/git
git clone https://github.com/fontforge/libspiro.git
cd libspiro
autoreconf -i
automake --foreign -Wall
./configure
make
sudo make install
cd ..

cd /home/vagrant/git
git clone https://github.com/fontforge/libuninameslist.git
cd libuninameslist
autoreconf -i
automake --foreign
./configure
make
sudo make install
cd ..
