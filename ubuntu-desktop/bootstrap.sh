#!/usr/bin/env bash

apt-get update
apt-get install -y build-essentials
apt-get install -y xfce4
apt-get install -y emacs-nox
apt-get install -y git
apt-get install -y glade
apt-get install -y default-jre
apt-get install -y default-jdk
apt-get install -y eclipse
apt-get install -y maven
